package com.example.departmentservice.controller;

import com.example.departmentservice.entity.Department;
import com.example.departmentservice.repo.DepartmentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/department")
public class DepartmentController {

    @Value("${server.port}")
    private String serverPort;

    @Autowired
    private DepartmentRepo departmentRepo;

    @PostMapping
    public Department save(@RequestBody Department department){
        return departmentRepo.save(department);
    }

    @GetMapping("/{id}")
    public Map<String, ?> getById(@PathVariable Long id){
        Department department = departmentRepo.findById(id).get();
        Map<String, Object> response =  new HashMap<>();
        response.put("id", department.getId());
        response.put("name", department.getName());
        response.put("serverPort", serverPort);
        return response;
    }

}
